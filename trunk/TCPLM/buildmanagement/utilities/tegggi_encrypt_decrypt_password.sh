#!/bin/ksh

# setting the file names
INPUT_FILE=input_encrypt.txt
DECRYPTED_FILE=output_decrypted.txt
ENCRYPTED_OUT_FILE=encrypt.dat
PRIVATE_KEY_FILE=tegggi_private_key.pem
PUBLIC_KEY_FILE=tegggi_public_key.pem

encryptPassword()
{
#****************************************************
	echo "Encrypting Password with public key  "
		openssl rsautl -encrypt -inkey $PUBLIC_KEY_FILE -pubin -in $INPUT_FILE -out $ENCRYPTED_OUT_FILE
		OUT=$?
		if [ $OUT -eq 0 ];then
			echo "Done!"
		else
			echo "Failed!"
		fi
#***************************************************
}
dencryptPassword()
{
#****************************************************
	echo "Decrypting Password with private key  "
		openssl rsautl -decrypt -inkey $PRIVATE_KEY_FILE -in $ENCRYPTED_OUT_FILE -out $DECRYPTED_FILE
		OUT=$?
		if [ $OUT -eq 0 ];then
			echo "Done!"
		else
			echo "Failed!"
		fi
#***************************************************
}
generatePrivateKey()
{
#****************************************************
	echo "Generating private key  "
		openssl genrsa -out $PRIVATE_KEY_FILE 1024
		OUT=$?
		if [ $OUT -eq 0 ];then
			echo "Done!"
		else
			echo "Failed!"
		fi
#***************************************************
}
generatePublicKey()
{
#****************************************************
	echo "Generating public key  "
		openssl rsa -in $PRIVATE_KEY_FILE -out $PUBLIC_KEY_FILE -outform PEM -pubout
		OUT=$?
		if [ $OUT -eq 0 ];then
			echo "Done!"
		else
			echo "Failed!"
		fi
#***************************************************
}
while true
do
clear
echo ""
echo "******  TeGGGI Password Encryption Decryption Script   ******"
echo ""
echo ""
echo "Select the component to be started:"
echo "	1	Encrypt Password with public key"
echo "	2	Test Decrypt Password with private key"
echo "	3	Generate Private Key"
echo "	4	Generate Public Key"
echo "	5	Exit"
echo "Please enter the choice" 
read CHOICE

if [ "$CHOICE" = "5" ]
then
	echo "Script will exit."
	echo ""
	echo "Tschuss!!!"
	echo ""	
	exit 0
fi


if [ "$CHOICE" != 1 -a "$CHOICE" != 2 -a "$CHOICE" != 3 -a "$CHOICE" != 4 -a "$CHOICE" != 5  ]
then
	echo "Wrong menu entry!!!"
	echo "Menu will appear in next 2 seconds"
	sleep 2
	continue
fi

case $CHOICE
in
1)	
	encryptPassword
;;
2)	
	dencryptPassword
;;
3)	
	generatePrivateKey
;;
4)	
	generatePublicKey
;;		
5)	
	echo "TEGGGI Encryption Script will exit."
	echo ""
	echo "Tschuss!!!"
	echo ""	
	exit 0;;
*) 	echo "Wrong menu entry!!!"
	exit 0
esac

echo "Press RETURN for the menu"
read key
done
exit 0
