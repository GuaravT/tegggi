#!/bin/ksh
######################################################################################################################
# This script is used to stop subscriptionmgrd
# Author: Menk Slot
# date: 2015-08-17
######################################################################################################################
# Change Nb     Who             Date            Comments
#
#
######################################################################################################################

{
echo "Trying to kill Subscription and Action Manager..."
	{
	pkill subscriptionmgr
	pkill actionmgrd
	}
}

echo "Done."
