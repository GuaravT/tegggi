@echo off

set TCROOT=C:\Siemens\Teamcenter9_test2T
set PATCH=D:\G Drive\Desktop\TC9.1patch\
set INST_PATCH=D:\G Drive\Desktop\TC9.1patch\Tc9.1.2_patch_5_install.zip

echo Current directory: %~dp0

set COMMAND=%TCROOT%\install\install\unzip -o %INST_PATCH% -d %TCROOT%\install
echo Running %COMMAND%
%COMMAND%


set COMMAND=%TCROOT%\install\tem.bat -p %PATCH%
echo Running %COMMAND%
%COMMAND%

echo "Please review the status of the update in the log file install_UPDATER_date.log in %TCROOT%/log"
