@echo off
set JAVA_HOME=D:\Java\jdk1.6
set PATH=%JAVA_HOME%\bin;%PATH%
set INSTALL_IMAGE_PATH=D:TC9.1dump
set SILENT_XML_FILE=silent.xml


echo Current directory: %~dp0
set COMMAND=%INSTALL_IMAGE_PATH%\tem.bat -s %SILENT_XML_FILE%
echo Running %COMMAND%
%COMMAND%
