#!/bin/bash
######################################################################################################################
# This script is used to run the utility list_users to get an overview of the logged in users
# This script will source the needed parameters from
# ../configurations/environment.properties
# Author: Menk Slot
# date: 2015-05-05
######################################################################################################################
# Change Nb     Who             Date            Comments
#
#
######################################################################################################################
# setting the source environment.properties file
source ../configurations/environment.properties
list_users -u=infodba -pf=$TC_USR_PASSWORD_FILE_LOC -g=dba

