#!/bin/sh

export TCROOT=/home/infodba/BaluWS/tc_bls
export PATCH=/BMW1/TC91_Linux_64/Tc9.1_lnx64_1_of_2
export INST_PATCH=/BMW1/TC91_Linux_64/Tc9.1_lnx64_1_of_2.zip


echo "Coping Install Folder"
$TCROOT\install\install\unzip -o $INST_PATCH -d $TCROOT\install
chmod 755 $TCROOT/install/tem.sh

echo "Started Applying Patch"
$TCROOT/install/tem.sh -p $PATCH


echo "Please review the status of the update in the log file install_UPDATER_date.log in $TCROOT/log"
