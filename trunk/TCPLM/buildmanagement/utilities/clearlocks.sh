#!/bin/bash
######################################################################################################################
# This script is used to set the parameters and run a clearlocks -verbose
# This script will source the needed parameters from
# ../configurations/environment.properties
# Author: Menk Slot
# date: 2015-05-05
######################################################################################################################
# Change Nb     Who             Date            Comments
#
#
######################################################################################################################
# setting the source environment.properties file
source ../configurations/environment.properties
clearlocks -verbose

