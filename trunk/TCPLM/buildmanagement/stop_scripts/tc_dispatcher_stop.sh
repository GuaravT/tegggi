#!/bin/ksh

# setting the source environment.properties file
source ../configurations/environment.properties

startScheduler()
{
#****************************************************
	echo "Starting Dispatcher Scheduler Service "
	 $DISP_ROOT/Scheduler/bin/runscheduler.sh &                 
	 echo "Done."
#****************************************************
}
startClient()
{
#****************************************************
	echo "Starting Dispatcher Client Service "
	 $DISP_ROOT/DispatcherClient/bin/runDispatcherClient.sh &                 
	 echo "Done."
#****************************************************
}
startModule()
{
#****************************************************
	echo "Starting Dispatcher Module Service "
	 $DISP_ROOT/Module/bin/runmodule.sh &                 
	 echo "Done."
#****************************************************
}
stopScheduler()
{
#****************************************************
	echo "Starting Dispatcher Scheduler Service "
	 $DISP_ROOT/Scheduler/bin/runscheduler.sh -stop &                 
	 echo "Done."
#****************************************************
}
stopClient()
{
#****************************************************
	echo "Starting Dispatcher Client Service "
	 $DISP_ROOT/DispatcherClient/bin/runDispatcherClient.sh -stop &                 
	 echo "Done."
#****************************************************
}
stopModule()
{
#****************************************************
	echo "Starting Dispatcher Module Service "
	 $DISP_ROOT/Module/bin/runmodule.sh -stop &                 
	 echo "Done."
#****************************************************
}
while true
do
clear
echo ""
echo "******  Dispatcher Service Stop Script   ******"
echo ""
echo " Configured DISP_ROOT location is "
echo $DISP_ROOT
echo ""
echo "Select the component to be started:"
echo "	1	Dispatcher Scheduler"
echo "	2	Dispatcher Client"
echo "	3	Dispatcher Module"
echo "	4	ALL"
echo "	5	Exit"
echo "Please enter the choice" 
read CHOICE

if [ "$CHOICE" = "5" ]
then
	echo "Script will exit."
	echo ""
	echo "Tschuss!!!"
	echo ""	
	exit 0
fi


if [ "$CHOICE" != 1 -a "$CHOICE" != 2 -a "$CHOICE" != 3 -a "$CHOICE" != 4 -a "$CHOICE" != 5  ]
then
	echo "Wrong menu entry!!!"
	echo "Menu will appear in next 2 seconds"
	sleep 2
	continue
fi

case $CHOICE
in
1)	
	stopScheduler
;;
2)	
	stopClient
;;
3)	
	stopModule
;;
4)	
	stopModule
	stopClient
	stopScheduler
;;		
5)	
	echo "TEGGGI Dispatcher Script will exit."
	echo ""
	echo "Tschuss!!!"
	echo ""	
	exit 0;;
*) 	echo "Wrong menu entry!!!"
	exit 0
esac

echo "Press RETURN for the menu"
read key
done
exit 0
