#!/bin/ksh

# setting the source environment.properties file
source ../configurations/environment.properties

ADMIN_ID=manager
ADMIN_PWD=manager
JMX_MANAGER_PORT=13740
POOL_ID=PoolA
LOGDIR=/scr/$TC_SERVER/tegggi/logs/$TC_ENV/pool_manager
CURRENT_DATE=`date +%Y%m%d-%H:%M`
LOGFILE=stop_poolmgr_${CURRENT_DATE}.log


#****************************************************
wget --delete-after --output-file=${LOGDIR}/${LOGFILE} --http-user=${ADMIN_ID} --http-password=${ADMIN_PWD} "http://localhost:${JMX_MANAGER_PORT}/InvokeAction//Administer+${POOL_ID}+manager+%3Aid%3D${POOL_ID}/action=Shutdown_Manager?action=Shutdown_Manager&Wait+For+Servers+To+Become+Idle%2Bjava.lang.Boolean=false"

echo "Pool Manager Stopped" 

cd  $LOGDIR
#****************************************************
