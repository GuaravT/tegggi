#!/bin/bash 
# setting the source environment.properties file
source ../configurations/environment.properties
# setting the source for build specific properties
source ./build.properties

export ANT_BIN=$ANT_BIN
export JAVA_HOME=$JAVA_HOME
export ECLIPSE_HOME=$ECLIPSE_HOME
export DESTINATION_LOCATION=$DESTINATION_LOCATION/"$RELEASE_NAME"-$(date +%Y_%m_%d)
export TEGGGI_CHECK_OUT_LOCATION=$TEGGGI_CHECK_OUT_LOCATION
export TEAMCENTER_SERVICES_HOME=$TEAMCENTER_SERVICES_HOME
export TC_ROOT=$TC_ROOT
export TC_LIB=$TC_ROOT/lib
export SSL_DIR=/usr/local/ssl/
export LD_LIBRARY_PATH=/usr/local/ssl/lib64:$LD_LIBRARY_PATH
export SVN_UPDATE=$SVN_UPDATE
export SOA_CLIENT_KIT=$SOA_CLIENT_KIT

echo $ANT_BIN
echo $JAVA_HOME
svnComplete()
{
#if svn_update is set to true and the folder exists, the code will be updated, otherwise it will create the folder and check out the code
	if [ "$SVN_UPDATE" = "true" -a -d "$TEGGGI_CHECK_OUT_LOCATION" ]; then
		echo Updating $TEGGGI_CHECK_OUT_LOCATION
		cd $TEGGGI_CHECK_OUT_LOCATION
		svn up --username=$SVN_USER_NAME --password=$SVN_PASSWORD
	else
		echo cleaning $TEGGGI_CHECK_OUT_LOCATION
		rm -rf $TEGGGI_CHECK_OUT_LOCATION/*
		svn co $SVN_URL --username=$SVN_USER_NAME --password=$SVN_PASSWORD --no-auth-cache $TEGGGI_CHECK_OUT_LOCATION
	fi
}
svnClientTier()
{
	if [ "$SVN_UPDATE" = "true" -a -d "$TEGGGI_CHECK_OUT_LOCATION/client-tier" ]; then
		echo Updating $TEGGGI_CHECK_OUT_LOCATION/client-tier
		cd $TEGGGI_CHECK_OUT_LOCATION/client-tier
		svn up --username=$SVN_USER_NAME --password=$SVN_PASSWORD
	else 
		mkdir -p $TEGGGI_CHECK_OUT_LOCATION/client-tier
		echo TeGGGI : Cleaning $TEGGGI_CHECK_OUT_LOCATION/client-tier
		rm -rf  $TEGGGI_CHECK_OUT_LOCATION/client-tier/*
		svn co $SVN_URL/client-tier --username=$SVN_USER_NAME --password=$SVN_PASSWORD --no-auth-cache $TEGGGI_CHECK_OUT_LOCATION/client-tier
	fi
}

svnServerTier()
{
	if [ "$SVN_UPDATE" = "true" -a -d "$TEGGGI_CHECK_OUT_LOCATION/server-tier" ]; then
		echo Updating $TEGGGI_CHECK_OUT_LOCATION/server-tier
		cd $TEGGGI_CHECK_OUT_LOCATION/server-tier
		svn up --username=$SVN_USER_NAME --password=$SVN_PASSWORD
	else
		echo "Start server tier"	
		mkdir -p $TEGGGI_CHECK_OUT_LOCATION/server-tier
		echo TeGGGI : Cleaning $TEGGGI_CHECK_OUT_LOCATION/server-tier
		rm -rf  $TEGGGI_CHECK_OUT_LOCATION/server-tier/*
		svn co $SVN_URL/server-tier --username=$SVN_USER_NAME --password=$SVN_PASSWORD --no-auth-cache $TEGGGI_CHECK_OUT_LOCATION/server-tier
	fi
}

svnWebTier()
{
	if [ "$SVN_UPDATE" = "true" -a -d "$TEGGGI_CHECK_OUT_LOCATION/web-tier" ]; then
		echo Updating $TEGGGI_CHECK_OUT_LOCATION/web-tier
		cd $TEGGGI_CHECK_OUT_LOCATION/web-tier
		svn up --username=$SVN_USER_NAME --password=$SVN_PASSWORD
	else
		mkdir -p $TEGGGI_CHECK_OUT_LOCATION/web-tier
		echo TeGGGI : Cleaning $TEGGGI_CHECK_OUT_LOCATION/web-tier
		rm -rf  $TEGGGI_CHECK_OUT_LOCATION/web-tier/*
		svn co $SVN_URL/web-tier --username=$SVN_USER_NAME --password=$SVN_PASSWORD --no-auth-cache $TEGGGI_CHECK_OUT_LOCATION/web-tier	
	fi
}

svnConfigurations()
{
	if [ "$SVN_UPDATE" = "true" -a -d "$TEGGGI_CHECK_OUT_LOCATION/configurations" ]; then
		echo Updating $TEGGGI_CHECK_OUT_LOCATION/configurations
		cd $TEGGGI_CHECK_OUT_LOCATION/configurations
		svn up --username=$SVN_USER_NAME --password=$SVN_PASSWORD
	else
		mkdir -p $TEGGGI_CHECK_OUT_LOCATION/configurations
		echo TeGGGI : Cleaning $TEGGGI_CHECK_OUT_LOCATION/configurations
		rm -rf  $TEGGGI_CHECK_OUT_LOCATION/configurations/*
		svn co $SVN_URL/configurations --username=$SVN_USER_NAME --password=$SVN_PASSWORD --no-auth-cache $TEGGGI_CHECK_OUT_LOCATION/configurations
	fi
}

svnGateway()
{
	if [ "$SVN_UPDATE" = "true" -a -d "$TEGGGI_CHECK_OUT_LOCATION/integrations/t4x" ]; then
		echo Updating $TEGGGI_CHECK_OUT_LOCATION/integrations/t4x
		cd $TEGGGI_CHECK_OUT_LOCATION/integrations/t4x
		svn up --username=$SVN_USER_NAME --password=$SVN_PASSWORD
	else
		mkdir -p $TEGGGI_CHECK_OUT_LOCATION/integrations/t4x
		echo TeGGGI : Cleaning $TEGGGI_CHECK_OUT_LOCATION/integrations/t4x
		rm -rf  $TEGGGI_CHECK_OUT_LOCATION/integrations/t4x/*
		svn co $SVN_URL/integrations/t4x --username=$SVN_USER_NAME --password=$SVN_PASSWORD --no-auth-cache $TEGGGI_CHECK_OUT_LOCATION/integrations/t4x
	fi
}

svnSoaServices()
{
	if [ "$SVN_UPDATE" = "true" -a -d "$TEGGGI_CHECK_OUT_LOCATION/configurations/01_BMIDE" ]; then
		echo Updating $TEGGGI_CHECK_OUT_LOCATION/configurations/01_BMIDE
		cd $TEGGGI_CHECK_OUT_LOCATION/configurations/01_BMIDE
		svn up --username=$SVN_USER_NAME --password=$SVN_PASSWORD
	else
		mkdir -p $TEGGGI_CHECK_OUT_LOCATION/configurations/01_BMIDE
		echo TeGGGI : Cleaning $TEGGGI_CHECK_OUT_LOCATION/configurations/01_BMIDE
		rm -rf  $TEGGGI_CHECK_OUT_LOCATION/configurations/01_BMIDE/*
		svn co $SVN_URL/configurations/01_BMIDE --username=$SVN_USER_NAME --password=$SVN_PASSWORD --no-auth-cache $TEGGGI_CHECK_OUT_LOCATION/configurations/01_BMIDE
	fi
	
}

clienttier()
{
	if [  -d "$DESTINATION_LOCATION/client-tier" ]; then
		cd $DESTINATION_LOCATION
		echo Cleaning $DESTINATION_LOCATION/client-tier
		rm -rf client-tier/*
	fi		
	mkdir -p $DESTINATION_LOCATION/client-tier/RAC/plugins
	mkdir -p $TEGGGI_CHECK_OUT_LOCATION/client-tier/RAC/build_directory
	
	$JAVA_HOME/bin/java -jar $ECLIPSE_HOME/plugins/org.eclipse.equinox.launcher_1.3.0.v20120522-1813.jar -application org.eclipse.ant.core.antRunner -buildfile $TEGGGI_CHECK_OUT_LOCATION/client-tier/RAC/build/build.xml
	
	cp $TEGGGI_CHECK_OUT_LOCATION/client-tier/RAC/plugins/plugins/*.jar $DESTINATION_LOCATION/client-tier/RAC/plugins/.
	echo OutputLocation is $DESTINATION_LOCATION/client-tier
}

servertier()
{
	if [  -d "$DESTINATION_LOCATION/server-tier" ]; then
		cd $DESTINATION_LOCATION
		echo Cleaning $DESTINATION_LOCATION/server-tier
		rm -rf server-tier/*
	fi	

	echo "Directory is:  " $DESTINATION_LOCATION/server-tier
	mkdir -p $DESTINATION_LOCATION/server-tier/lib $DESTINATION_LOCATION/server-tier/obj $DESTINATION_LOCATION/server-tier/bin $DESTINATION_LOCATION/server-tier/lang
	$ANT_BIN -f $TEGGGI_CHECK_OUT_LOCATION/server-tier/build/build.xml -l $DESTINATION_LOCATION/server-tier/Logbuild_$(date +%Y_%m_%d).log -verbose
	
	cp -r $TEGGGI_CHECK_OUT_LOCATION/server-tier/src/bmw/lib/*.so $DESTINATION_LOCATION/server-tier/lib
	cp -r $TEGGGI_CHECK_OUT_LOCATION/server-tier/src/bmw/3rd_party_libs/*.so $DESTINATION_LOCATION/server-tier/lib
    cp -r $TEGGGI_CHECK_OUT_LOCATION/server-tier/lang $DESTINATION_LOCATION/server-tier
	chmod -R 755 $DESTINATION_LOCATION
	
	echo OutputLocation is $DESTINATION_LOCATION/server-tier
}

webtier()
{
	if [  -d "$DESTINATION_LOCATION/web-tier" ]; then
		cd $DESTINATION_LOCATION
		echo Cleaning $DESTINATION_LOCATION/web-tier
		rm -rf web-tier/*
	fi
	
	mkdir -p $DESTINATION_LOCATION/web-tier/lib

	dos2unix $TEGGGI_CHECK_OUT_LOCATION/web-tier/T4B_TeGGGI_build_web_tier.sh
	chmod 777 $TEGGGI_CHECK_OUT_LOCATION/web-tier/T4B_TeGGGI_build_web_tier.sh
	$TEGGGI_CHECK_OUT_LOCATION/web-tier/T4B_TeGGGI_build_web_tier.sh
	
	echo OutputLocation is $DESTINATION_LOCATION/web-tier
}

configurations()
{
	if [  -d "$DESTINATION_LOCATION/configurations" ]; then
		cd $DESTINATION_LOCATION
		echo Cleaning $DESTINATION_LOCATION/configurations
		rm -rf configurations/*
	fi

	mkdir -p $DESTINATION_LOCATION/configurations	
	chmod -R 777 $DESTINATION_LOCATION/configurations
	cp -r $TEGGGI_CHECK_OUT_LOCATION/configurations/* $DESTINATION_LOCATION/configurations/
	echo OutputLocation is $DESTINATION_LOCATION/configurations

}

gateway()
{
	if [  -d "$DESTINATION_LOCATION/integrations/t4x" ]; then
		cd $DESTINATION_LOCATION
		echo Cleaning $DESTINATION_LOCATION/integrations/t4x
		rm -rf integrations/t4x/*
	fi

	mkdir -p $DESTINATION_LOCATION/integrations/t4x/gs
	
	dos2unix $TEGGGI_CHECK_OUT_LOCATION/integrations/t4x/gs/make_mmap.sh
	chmod 777 $TEGGGI_CHECK_OUT_LOCATION/integrations/t4x/gs/make_mmap.sh
	#setting TP_T4XINSTL where gs batch is installed so make_mmap can be executed
    TP_T4XINSTL=$TC_ROOT/t4x_root/t4x_gs
    export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$TP_T4XINSTL/lib64
	$TEGGGI_CHECK_OUT_LOCATION/integrations/t4x/gs/make_mmap.sh
	
	cp -r $TEGGGI_CHECK_OUT_LOCATION/integrations/t4x/gs/etc $DESTINATION_LOCATION/integrations/t4x/gs
	cp -r $TEGGGI_CHECK_OUT_LOCATION/integrations/t4x/gs/lib $DESTINATION_LOCATION/integrations/t4x/gs
	cp -r $TEGGGI_CHECK_OUT_LOCATION/integrations/t4x/gs/var $DESTINATION_LOCATION/integrations/t4x/gs
	cp -r $TEGGGI_CHECK_OUT_LOCATION/integrations/t4x/gs/make_mmap.sh $DESTINATION_LOCATION/integrations/t4x/gs/
	echo OutputLocation is $DESTINATION_LOCATION/integrations/t4x/gs
}

soaServices()
{
	if [  -d "$DESTINATION_LOCATION/soa-services" ]; then
		cd $DESTINATION_LOCATION
		echo Cleaning $DESTINATION_LOCATION/soa-services
		rm -rf soa-services/*
	fi	

	echo "Directory is:  " $DESTINATION_LOCATION/soa-services
	mkdir -p $DESTINATION_LOCATION/soa-services/lib
	
	cd $TEGGGI_CHECK_OUT_LOCATION/configurations/01_BMIDE/
	for D in *; do
		if [ -d "${D}" ]; then
			cd $TEGGGI_CHECK_OUT_LOCATION/configurations/01_BMIDE/$D
			if [ -r "makefile.lnx64" ]; then
				make -f makefile.lnx64 clean
                		make -f makefile.lnx64
                		cp -r $TEGGGI_CHECK_OUT_LOCATION/configurations/01_BMIDE/${D}/output/lnx64/lib/*.so  $DESTINATION_LOCATION/soa-services/lib
	            	fi
		fi	
	done
    chmod -R 755 $DESTINATION_LOCATION
	
	echo OutputLocation is $DESTINATION_LOCATION/soa-services
}

while true
do
clear
echo ""
echo "******  TEGGGI SCM Build Utility  ******"
echo ""
echo "Select the component to be build up:"
echo "	1	Client-Tier"
echo "	2	Server-Tier"
echo "	3	Web-Tier"
echo "	4	Configurations"
echo "	5	Gateway"
echo "	6	SOA Services"
echo "	7	ALL"
echo "  8   Exit"
echo "Please enter the choice" 
read CHOICE

if [ "$CHOICE" = "8" ]
then
	echo "TEGGGI SCM Build Utility will exit."
	echo ""
	exit 0
fi


if [ "$CHOICE" != 1 -a "$CHOICE" != 2 -a "$CHOICE" != 3 -a "$CHOICE" != 4 -a "$CHOICE" != 5 -a "$CHOICE" != 6 -a "$CHOICE" != 7 -a "$CHOICE" != 8 ]
then
	echo "Wrong menu entry!!!"
	echo "Menu will appear in next 2 seconds"
	sleep 2
	continue
fi

case $CHOICE
in
1)	
	svnClientTier
	clienttier
;;
2)	
	svnServerTier	
	servertier
;;
3)	
	svnWebTier	
	webtier	
;;
4)	
	svnConfigurations	
	configurations
;;		
5)	
	svnGateway	
	gateway
;;	
6)
	svnSoaServices
	soaServices
;;
7)	
	svnComplete
	configurations
	soaServices
	clienttier
	servertier
	webtier
	gateway
;;
8)	
	echo "TEGGGI SCM Build Utility will exit."
	echo ""
	exit 0
;;
*) 	echo "Wrong menu entry!!!"
	exit 0
esac

echo "Press RETURN for the menu"
read key
done
exit 0
