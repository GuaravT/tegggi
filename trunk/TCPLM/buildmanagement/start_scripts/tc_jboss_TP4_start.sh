#!/bin/ksh
# setting the source environment.properties file
source ../configurations/environment.properties

LOGDIR=/scr/$TC_SERVER/tegggi/logs/$TC_ENV/tp4_was/Jboss
INSTDIR=/share/$TC_SERVER/tegggi/$TC_ENV/jboss_7.1_tp4

PATH=$JAVA_HOME/bin:$PATH
START_EXEC="void"
CURRENT_DATE=`date +%Y%m%d-%H:%M`
LOGFILE=jboss_${CURRENT_DATE}.log



cd  $LOGDIR
#****************************************************
echo "Trying to start JBoss..."

chk_proc=`ps -ef | grep /$TC_ENV/jboss_7.1_tp4 | grep $RUN_USER | grep $TC_ENV | grep -v grep | wc -l`

if [ $chk_proc -ne 0 ]
then
	{
		echo "JBoss is alrady running."
	}
else
	{
		echo "Starting JBoss."
		nohup ${INSTDIR}/bin/standalone.sh -Djboss.socket.binding.port-offset=40 -b 0.0.0.0 -DLOGDIR=$LOGDIR > ${LOGDIR}/${LOGFILE} 2>&1 &
		echo "Logfile is ${LOGDIR}/${LOGFILE}"
	}
fi
#****************************************************
