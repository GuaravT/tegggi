#!/bin/ksh

# setting the source environment.properties file
source ../configurations/environment.properties

PATH=/share/$TC_SERVER/tegggi/$TC_ENV/jre1.7.0_71/bin:$PATH
param=$1
LOGDIR=/scr/$TC_SERVER/tegggi/logs/$TC_ENV/tp4_was/Jboss
INSTDIR=/share/$TC_SERVER/tegggi/$TC_ENV/jboss_7.1_tp4
START_EXEC="void"
CURRENT_DATE=`date +%Y%m%d-%H:%M`
LOGFILE=jboss_${CURRENT_DATE}.log


cd  $LOGDIR
#****************************************************
echo "Stopping JBoss...."

${INSTDIR}/bin/jboss-cli.sh --connect --controller=127.0.0.1:10039 command=:shutdown

#****************************************************


