#!/bin/ksh
# setting the source environment.properties file
source ../configurations/environment.properties

PATH=/share/$TC_SERVER/tegggi/$TC_ENV/jdk1.7.0_75/bin:$PATH
LOGDIR=/scr/$TC_SERVER/tegggi/logs/$TC_ENV/web_was/Jboss
INSTDIR=/share/$TC_SERVER/tegggi/$TC_ENV/jboss_7.1_web
JBOSS_HOME=/share/$TC_SERVER/tegggi/$TC_ENV/jboss_7.1_web


START_EXEC="void"
CURRENT_DATE=`date +%Y%m%d-%H:%M`
LOGFILE=jboss_${CURRENT_DATE}.log


cd  $LOGDIR
#****************************************************
echo "Trying to start JBoss..."

chk_proc=`ps -ef | grep /${TC_ENV}/jboss_7.1_web | grep ${RUN_USER} | grep ${TC_ENV} | grep -v grep | wc -l`

if [ $chk_proc -ne 0 ]
then
	{
		echo "JBoss is already running."
	}
else
	{
		echo "Starting JBoss."
		nohup ${INSTDIR}/bin/standalone.sh -b 0.0.0.0 > ${LOGDIR}/${LOGFILE} 2>&1 &
		echo "Logfile is ${LOGDIR}/${LOGFILE}"
	}
fi
#****************************************************
