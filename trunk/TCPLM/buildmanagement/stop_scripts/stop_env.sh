#!/bin/ksh
# setting the source environment.properties file
source ../configurations/environment.properties
#****************************************************

echo "Stoping two tier client..."
sh ./tc_two_tier_stop.sh
echo "Done."

echo "Stopping Pool Manager..." 
sh ./tc_poolmgr_stop.sh
echo "Done."

echo "Stopping Thin client JBoss..."
sh ./tc_jboss_stop.sh
echo "Done."

echo "Stopping SSO JBoss..."
sh ./tc_jboss_sso_stop.sh
echo "Done."
