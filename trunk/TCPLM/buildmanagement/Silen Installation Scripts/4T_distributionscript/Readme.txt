*************************************************************************************************************
4T Package Distributable ReadMe
*************************************************************************************************************
Set below variables in the batch file
1. TC_ROOT : Location where the 4T Client will be installed.
   e.g TC_ROOT=C:\EC-Apps\tc\tegggi_tc_test_03\tc\9.1. 
   As per the BMW standards this has to be C:\EC-Apps\tc constant for all the environments
   tegggi_<environment name>\tc\9.1
 
2.TC_4T_CLIENT_LOC_DEST 
   Location for 4T installation. This has to be again as given below. Please don't change the value.
  e.g.TC_4T_CLIENT_LOC_DEST=C:\EC-Apps\tc
3. TC_4T_CLIENT_LOC_SRC Location where the distributable zip is located.
e.g TC_4T_CLIENT_LOC_SRC=L:\proj\tegggi\TC_Clients\14.1
4. Name of the ZIP file.
e.g.FILE_NAME=tegggi_tc_test_03_32bit_without_Siteminder.zip
*************************************************************************************************************