#!/bin/sh
export TCROOT=/home/infodba/Akshay/Siemens/Teamcenter9
export PATCH=/BMW/Tc9.1_lnx64_1_of_2/Tc9.1.2_patch_5_wntx64
export INST_PATCH=/BMW/Tc9.1_lnx64_1_of_2/Tc9.1.2_patch_5_wntx64/wntx64

echo "Coping Install Folder"
$TCROOT/install/install/unzip -o $INST_PATCH -d $TCROOT/install
chmod 755 $TCROOT/install/tem.sh
echo "Started Applying Patch"

$TCROOT/install/tem.sh -p $PATCH


echo "Please review the status of the update in the log file install_UPDATER_date.log in $TCROOT/log"
