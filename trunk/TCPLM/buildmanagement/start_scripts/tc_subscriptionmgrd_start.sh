#!/bin/ksh
######################################################################################################################
# This script is used to start subscriptionmgrd
# This script will source the needed parameters from
# ../configurations/environment.properties
# Author: Menk Slot
# date: 2015-07-16
######################################################################################################################
# Change Nb     Who             Date            Comments
#
#
######################################################################################################################
# setting the source environment.properties file
source ../configurations/environment.properties

#****************************************************
echo "Trying to start Subscriptionmanager..."

#Check if subscriptionmanager is already running
chk_proc_sub=`ps -ef | grep qqtegi | grep bin'/'subscriptionmgrd|grep -v grep |  wc -l`

if [ $chk_proc_sub -ne 0 ]
then
	{
		echo "Subscriptionmanager is already running."
	}
else
	{
		echo "Starting SubscriptionManager."
                $TC_ROOT/bin/subscriptionmgrd -u=infodba -pf=$TC_USR_PASSWORD_FILE_LOC  -g=dba > $TC_TMP_DIR/subscriptionmgr.log &
	}
fi

echo "trying to start actionmanager...."


#Check if actionmanager is already running
chk_proc_action=`ps -ef | grep qqtegi | grep bin'/'actionmgrd|grep -v grep |  wc -l`

if [ $chk_proc_action -ne 0 ]
then
        {
                echo "Actionmanager is already running."
        }
else
        {
                echo "Starting ActionManager."
                $TC_ROOT/bin/actionmgrd -u=infodba -pf=$TC_USR_PASSWORD_FILE_LOC  -g=dba > $TC_TMP_DIR/actionmgr.log &
        }
fi

echo "Check if Subscription and Action manager are running"
chk_proc_sub=`ps -ef | grep qqtegi | grep bin'/'subscriptionmgrd|grep -v grep |  wc -l`

if [ $chk_proc_sub -ne 0 ]
then
        {
                echo "Subscriptionmanager is started succesfully."
        }
else
        {
                echo "There is a problem with starting SubscriptionManager"
        }
fi

chk_proc_action=`ps -ef | grep qqtegi | grep bin'/'actionmgrd|grep -v grep |  wc -l`

if [ $chk_proc_action -ne 0 ]
then
        {
                echo "Actionmanager is started succesfully."
        }
else
        {
                echo "There is a problem with starting ActionManager."
        }
fi

                                                                      
echo "Done."

#****************************************************
