#!/bin/bash
######################################################################################################################
# This script will be used for deploy changes to an environment
# This script will source the needed parameters from
# ../configurations/environment.properties and deploy.properties
# Author: Menk Slot
# date: 2015-05-04
######################################################################################################################
# Change Nb     Who             Date            Comments
# 01			Menk Slot		20150729		Changed copy statement at server-tier based on tc_integ
#
######################################################################################################################

# setting the source environment.properties file
source ../configurations/environment.properties
# setting the source for deploy specific properties
source ./deploy.properties

export SRC_BUILD_DIR=$SRC_BUILD_DIR
export TC_ROOTS=$TC_ROOT_LOC
export TC_DATA=$TC_DATA
export SOA_LIB_LOCATION=$SOA_LIB_LOCATION
export TC_IMPORT_USER=$TC_IMPORT_USER
export TC_USR_PASSWORD_FILE_LOC=$TC_USR_PASSWORD_FILE_LOC
export TC_IMPORT_USER_GROUP=$TC_IMPORT_USER_GROUP
export TP4_JBOSS_HOMES=$TP4_JBOSS_HOME
export LOG_DIR_NAME=LOG_DIR_$(date +%Y_%m_%d_%H_%M_%S)
export TC_ROOT=$TC_ROOT
# added definition FMS_HOME 20150220 Menk Slot
export FMS_HOME=$FMS_HOME
# Changes for Character sets d.d. 20141127 U. Brauner
# Changed settings LANG="en_US" based on testing this in tc_test_02
# 2014-12-16 Menk Slot
# LANG="en_US.UTF-8" ; export LANG
LANG="en_US" ; export LANG
# LC_CTYPE="en_US.UTF-8" ; export LC_CTYPE
# LC_NUMERIC="en_US.UTF-8" ; export LC_NUMERIC
# LC_TIME="en_US.UTF-8" ; export LC_TIME
# LC_COLLATE="en_US.UTF-8" ; export LC_COLLATE
# LC_MONETARY="en_US.UTF-8" ; export LC_MONETARY
# LC_MESSAGES="en_US.UTF-8" ; export LC_MESSAGES
# LC_PAPER="en_US.UTF-8" ; export LC_PAPER
# LC_NAME="en_US.UTF-8" ; export LC_NAME
# LC_ADDRESS="en_US.UTF-8" ; export LC_ADDRESS
# LC_TELEPHONE="en_US.UTF-8" ; export LC_TELEPHONE
# LC_MEASUREMENT="en_US.UTF-8" ; export LC_MEASUREMENT
# LC_IDENTIFICATION="en_US.UTF-8" ; export LC_IDENTIFICATION
unset LC_ALL
# End Changes for Character sets

mkdir $SRC_BUILD_DIR/$LOG_DIR_NAME
export LOG_DIR=$SRC_BUILD_DIR/$LOG_DIR_NAME
	

export IFS=";"

export T4X_GS_ROOTS=($TC_ROOT/t4x_gs $TC_ROOT/t4x_gs_batch)

clienttier()
{	
	
	#for tc_root in `echo $TC_ROOTS|tr";" "\n"´ ; do
	for tc_root in $TC_ROOTS; do
		if [ -d $SRC_BUILD_DIR/client-tier/RAC/plugins/plugins ] ; then
			cp -r $SRC_BUILD_DIR/client-tier/RAC/plugins/plugins/* $tc_root/portal/plugins/.
			cp -r $SRC_BUILD_DIR/client-tier/RAC/3rd_party_libs/* $tc_root/portal/plugins/.
			cd $tc_root/portal/registry
			sh genregxml.sh
		else 
			echo "Nothing to deploy for client tier"
		fi
	done
}

servertier()
{

	echo $TC_ROOTS

	for ppath in `echo $TC_ROOTS |tr ":" "\n"`; do 
		echo $ppath
		tmp_loc=$ppath
		if [ -d $SRC_BUILD_DIR/server-tier/lib ] ; then
			cp -r $SRC_BUILD_DIR/server-tier/lib $TC_ROOT
			chmod 755 $TC_ROOT/lib
		else
			echo "Please check the structure folder of server-tier, the libs were not copied to $TC_ROOT"
		fi
		if [ -d $SRC_BUILD_DIR/server-tier/lang/textserver ] ; then
			cp -r $SRC_BUILD_DIR/server-tier/lang $TC_ROOT
		else
			echo "Please check the structure folder of server-tier, the lang folder was not copied to $TC_ROOT"
		fi
		#sao services libs
		if [ -d $SRC_BUILD_DIR/soa-services/lib ] ; then
			cp -r $SRC_BUILD_DIR/soa-services/lib/* $TC_ROOT/lib/.
			chmod 755 $TC_ROOT/lib
		fi
	done
	
	##### Dispatcher binary deployment 
	####### For custom translators binary add required deploy steps here s#####
}

webtier()
{
	if [ -d $SRC_BUILD_DIR/web-tier/lib ]
	then 
		cp -r $SRC_BUILD_DIR/web-tier/lib/* $SOA_LIB_LOCATION
	else
		echo "Please check the structure folder of web-tier, the libs were not copied to $SOA_LIB_LOCATION"
	fi
	for jboss_home in $TP4_JBOSS_HOMES; do
		if [ ! -d  $jboss_home/standalone/deployments ] ; then
			cp -r $SRC_BUILD_DIR/web-tier/TeGGGIWebService.war $jboss_home/standalone/deployments/.
		else
			echo "No jboss TP4 found"
		fi
	done
}

configurations()
{
	#2
	cd $SRC_BUILD_DIR/configurations/02_Preferences
	dos2unix T4B_TeGGGI_Preference_Import_Command.sh
	sh T4B_TeGGGI_Preference_Import_Command.sh
	
	#3
	#cd $SRC_BUILD_DIR/configurations/03_Organization
	#dos2unix T4B_TeGGGI_Organization_Import_Command.sh
	#sh T4B_TeGGGI_Organization_Import_Command.sh
	
	#4
	cd $SRC_BUILD_DIR/configurations/04_Ruletree
	dos2unix T4B_TeGGGI_ACL_Import_Command.sh
	sh T4B_TeGGGI_ACL_Import_Command.sh
	
	#6
	#cd $SRC_BUILD_DIR/configurations/06_Classification
	#dos2unix H4B_VR2.0_Classification_Import_Command.sh
	# sh H4B_VR2.0_Classification_Import_Command.sh
	
	#7
	cd $SRC_BUILD_DIR/configurations/07_Queries
	dos2unix T4B_TeGGGI_Query_Import_Command.sh
	sh T4B_TeGGGI_Query_Import_Command.sh
	
	#8
	cd $SRC_BUILD_DIR/configurations/08_Transfermode
	dos2unix T4B_TeGGGI_TransferMode_Import_Command.sh
	sh T4B_TeGGGI_TransferMode_Import_Command.sh
	
	#9
	cd $SRC_BUILD_DIR/configurations/09_Workflows
	dos2unix T4B_TeGGGI_Workflow_Import_Command.sh
	sh T4B_TeGGGI_Workflow_Import_Command.sh
	
	#10 
	cd $SRC_BUILD_DIR/configurations/10_XMLStylesheets
	dos2unix T4B_TeGGGI_StyleSheets_Import_Command.sh
	sh T4B_TeGGGI_StyleSheets_Import_Command.sh
	
	#16
	cd $SRC_BUILD_DIR/configurations/16_Reports
	dos2unix T4B_TeGGGI_Report_Import_Command.sh
	sh T4B_TeGGGI_Report_Import_Command.sh
	
	#19
	#cd $SRC_BUILD_DIR/configurations/19_RevisionRules
	#dos2unix T4B_TeGGGI_RevisionRule_Import_Command.sh
	#sh T4B_TeGGGI_RevisionRule_Import_Command.sh
	
	#25
	cd $SRC_BUILD_DIR/configurations/25_CommandSuppressions
	dos2unix T4B_TeGGGI_Command_Suppression_Preference_Import_Command.sh
	sh T4B_TeGGGI_Command_Suppression_Preference_Import_Command.sh
	
	#26
	cd $SRC_BUILD_DIR/configurations/26_Hiddenperspectives
	dos2unix T4B_TeGGGI_Preference_Import_Command.sh
	sh T4B_TeGGGI_Preference_Import_Command.sh
	
	#32
	#### Deploy Dispatcher Configurations 
	### For TeGGGI 15.20 no configuration change to OOTB so no need to deploy
	### Preference 
	#cd $SRC_BUILD_DIR/configurations/32_Dispatcher/Preferences
	#dos2unix T4B_TeGGGI_Preference_Import_Command.sh
	#sh T4B_TeGGGI_Preference_Import_Command.sh
	#### For script and property deployment please add required steps.
	###
}

gateway()
{
	 for CURRENT_T4X_ROOT in ${T4X_GS_ROOTS[*]} ; do
		if [ -r $SRC_BUILD_DIR/integrations/t4x/gs/make_mmap.sh ] ; then
			cp -vr $SRC_BUILD_DIR/integrations/t4x/gs/make_mmap.sh $CURRENT_T4X_ROOT/.
		fi
		if [ -d $SRC_BUILD_DIR/integrations/t4x/gs/lib ] ; then
			cp -vr $SRC_BUILD_DIR/integrations/t4x/gs/lib/* $CURRENT_T4X_ROOT/lib/.
		else 
			echo "Please check the structure folder of integrations, the libs were not copied to $CURRENT_T4X_ROOT/t4x_root"
		fi
		if [ -d $SRC_BUILD_DIR/integrations/t4x/gs/etc ] ; then
			cp -vr $SRC_BUILD_DIR/integrations/t4x/gs/etc/* $CURRENT_T4X_ROOT/etc/.
		fi
		if [ -d $SRC_BUILD_DIR/integrations/t4x/gs/var ] ; then
			cp -vr $SRC_BUILD_DIR/integrations/t4x/gs/var/init/* $CURRENT_T4X_ROOT/var/init/.
			cp -vr $SRC_BUILD_DIR/integrations/t4x/gs/var/lang/* $CURRENT_T4X_ROOT/var/lang/.
			cp -vr $SRC_BUILD_DIR/integrations/t4x/gs/var/mmap/* $CURRENT_T4X_ROOT/var/mmap/.
			cp -vr $SRC_BUILD_DIR/integrations/t4x/gs/var/test/* $CURRENT_T4X_ROOT/var/test/.
		else 
			echo "Please check the structure folder of integrations, the var sub-folders were not copied to $TC_ROOT/t4ea_root"
		fi
	done
}

while true
do
clear
echo ""
echo "******  TEGGGI SCM Deployment Utility  ******"
echo ""
echo "Select the component to be deployed:"
echo "	1	Client-Tier"
echo "	2	Server-Tier"
echo "	3	Web-Tier"
echo "	4	Configurations"
echo "  5   Gateway"
echo "	6	ALL"
echo "	7	Exit"
echo "Please enter the choice" 
read CHOICE

if [ "$CHOICE" = "7" ]
then
	echo "TEGGGI SCM Deployment Utility will exit."
	echo ""
	exit 0
fi

if [ "$CHOICE" != 1 -a "$CHOICE" != 2 -a "$CHOICE" != 3 -a "$CHOICE" != 4 -a "$CHOICE" != 5 -a "$CHOICE" != 6 -a "$CHOICE" != 7 ]
then
	echo "Wrong menu entry!!!"
	echo "Menu will appear in next 2 seconds"
	sleep 2
	continue
fi

case $CHOICE
in
1)	clienttier
;;
2)	servertier
;;
3)	webtier	
;;
4)	configurations
;;		
5)	gateway
;;
6)	clienttier
	servertier
	webtier
	configurations
	gateway
;;
7)	echo "TEGGGI SCM Deployment Utility will exit."
	echo ""	
	exit 0
;;
*) 	echo "Wrong menu entry!!!"
	exit 0
esac

echo "Press RETURN for the menu"
read key
done
exit 0
