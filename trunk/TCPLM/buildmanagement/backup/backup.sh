#!/bin/bash
######################################################################################################################
# This script can be used to make a backup of TC_ROOT, TC_DATA and the Volumes
# This script will source the needed parameters from
# ../configurations/environment.properties
# Author: Menk Slot
# date: 2015-04-28
######################################################################################################################
# Change Nb	Who		Date		Comments
#
#
######################################################################################################################
# setting the source environment.properties file
source ../configurations/environment.properties


DATE=$(date)
while true
do
TIME_STAMP=$(date +%Y%m%d)
clear
echo ""
echo "******  TEGGGI Backup Utility  ******"
echo ""
echo " Your backup will be stored on : " $BACKUP_DIR/$TIME_STAMP
echo ""
echo "Select the component to be backed up:"
echo "	1	TC Root"
echo "	2	TC Data"
echo "	3	Volume1 and Volume2"
echo "	4	Both TC Root and TC Data"
echo "	5	All TC Root, TC Data and all the volumes"
echo "	6	Exit"
echo "Please enter the choice" 
read CHOICE

if [ "$CHOICE" = "6" ]
then
	echo "TEGGGI Backup Utility will exit."
	echo ""
	echo "Tschuss!!!"
	echo ""	
	exit 0
fi

if [ "$CHOICE" != 1 -a "$CHOICE" != 2 -a "$CHOICE" != 3 -a "$CHOICE" != 4 -a "$CHOICE" != 5 -a "$CHOICE" != 6 ]
then
	echo "Wrong menu entry!!!"
	echo "Menu will appear in next 2 seconds"
	sleep 2
	continue
fi


case $CHOICE
in
1)	if [ -d "$BACKUP_DIR" ]
	then
		echo $TC_ROOT
		echo "TC Root backup is starting"
		cd $BACKUP_DIR
		mkdir -p $TIME_STAMP
		chmod 777 $TIME_STAMP
		cd $TC_ROOT
		echo "TC Root back up in progress..."
		tar -cf $BACKUP_DIR/$TIME_STAMP//TC_ROOT.tar *
		echo "TC Root backup is finished"
	else
		echo "The BACK UP directory does not exist."
	fi
;;	

2) 	if [ -d "$BACKUP_DIR" ]
	then
		echo "TC Data backup is starting"
		cd $BACKUP_DIR
		mkdir -p $TIME_STAMP
		chmod 777 $TIME_STAMP
		cd $TC_DATA
		echo "TC Data back up in progress..."
		tar -cf $BACKUP_DIR/$TIME_STAMP//TC_DATA.tar *
		echo "TC Data backup is finished"
	else
		echo "The BACK UP directory does not exist."
	fi
;;
3) 	if [ -d "$BACKUP_DIR" ]
	then
		cd $BACKUP_DIR
		mkdir -p $TIME_STAMP
		chmod 777 $TIME_STAMP
		cd $VOLUME_1
		echo "Volume 1 back up in progress..."
		tar -cf $BACKUP_DIR/$TIME_STAMP//VOLUME_1.tar *
		echo "Volume 1 backup is finished"
		echo "Volume 2 backup is starting"
		cd $VOLUME_2
		echo "Volume 2 back up in progress..."
		tar -cf $BACKUP_DIR/$TIME_STAMP//VOLUME_2.tar *
		echo "Volume 2 backup is finished"
	else
		echo "The BACK UP directory does not exist."
	fi
;;		
4) 	if [ -d "$BACKUP_DIR" ]
	then
		cd $BACKUP_DIR
		mkdir -p $TIME_STAMP
		chmod 777 $TIME_STAMP
		cd $TC_ROOT
		echo "TC Root back up in progress..."
		tar -cf $BACKUP_DIR/$TIME_STAMP//TC_ROOT.tar *
		echo "TC Root backup is finished"
		echo "TC Data backup is starting"
		cd $TC_DATA
		echo "TC Data back up in progress..."
		tar -cf $BACKUP_DIR/$TIME_STAMP//TC_DATA.tar *
		echo "TC Data backup is finished"
	else
		echo "The BACK UP directory does not exist."
	fi
;;
5) 	if [ -d "$BACKUP_DIR" ]
	then
		cd $BACKUP_DIR
		mkdir -p $TIME_STAMP
		chmod 777 $TIME_STAMP
		cd $TC_ROOT
		echo "TC Root back up in progress..."
		tar -cf $BACKUP_DIR/$TIME_STAMP//TC_ROOT.tar *
		echo "TC Root backup is finished"
		echo "TC Data backup is starting"
		cd $TC_DATA
		echo "TC Data back up in progress..."
		tar -cf $BACKUP_DIR/$TIME_STAMP//TC_DATA.tar *
		echo "TC Data backup is finished"
		cd $VOLUME_1
		echo "Volume 1 back up in progress..."
		tar -cf $BACKUP_DIR/$TIME_STAMP//VOLUME_1.tar *
		echo "Volume 1 backup is finished"
		echo "Volume 2 backup is starting"
		cd $VOLUME_2
		echo "Volume 2 back up in progress..."
		tar -cf $BACKUP_DIR/$TIME_STAMP//VOLUME_2.tar *
		echo "Volume 2 backup is finished"
	else
		echo "The BACK UP directory does not exist."
	fi
;;			
6)	echo "TEGGGI Backup Utility will exit."
	echo ""
	echo "Tschuss!!!"
	echo ""	
	exit 0;;
*) 	echo "Wrong menu entry!!!"
	exit 0
esac


echo "Press RETURN for the menu"
read key
done
exit 0
