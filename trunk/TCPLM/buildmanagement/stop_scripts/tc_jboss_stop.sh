#!/bin/ksh
# setting the source environment.properties file
source ../configurations/environment.properties

param=$1

PATH=$JAVA_HOME/bin:$PATH
LOGDIR=/scr/$TC_SERVER/tegggi/logs/$TC_ENV/web_was
INSTDIR=/share/$TC_SERVER/tegggi/$TC_ENV/jboss_7.1_web
START_EXEC="void"
CURRENT_DATE=`date +%Y%m%d-%H:%M`
LOGFILE=jboss_${CURRENT_DATE}.log


cd  $LOGDIR
#****************************************************

echo "Stopping JBoss...."
${INSTDIR}/bin/jboss-cli.sh --connect --controller=127.0.0.1:9999 command=:shutdown

#****************************************************


