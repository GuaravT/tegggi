#!/bin/ksh

# setting the source environment.properties file
source ../configurations/environment.properties

LOGDIR=/scr/$TC_SERVER/tegggi/logs/$TC_ENV/pool_manager
INSTDIR=$TC_ROOT/pool_manager/confs/${TC_ENV}_Tc10
START_EXEC=rc.tc.mgr_${TC_ENV}_Tc10_PoolA
CURRENT_DATE=`date +%Y%m%d-%H:%M`
LOGFILE=poolmgr_${CURRENT_DATE}.log


# TC_KEEP_SYSTEM_LOG=true          ; export TC_KEEP_SYSTEM_LOG

# TC_SLOW_SQL=1.0                  ; export TC_SLOW_SQL
# TC_DEBUG=ON                      ; export TC_DEBUG
# TC_TRACEBACK=ON                  ; export TC_TRACEBACK
# TC_SQL_DEBUG=BJPT                ; export TC_SQL_DEBUG
# TC_LOGGER_CONFIGURATION=/proj/tegggi/configuration/tc_test_07/tc10.1/tcdata/logger.debug.properties ; export TC_LOGGER_CONFIGURATION
# TC_PERFORMANCE_MONITOR=ON        ; export TC_PERFORMANCE_MONITOR
# TC_JOURNAL=SUMMARY                 ; export TC_JOURNAL
# TC_JOURNAL_MODULES=ALL           ; export TC_JOURNAL_MODULES
# TC_JOURNAL_MODULES="BOM EPM WSOM AOM"    ; export TC_JOURNAL_MODULES
# TC_POM_JOURNALLING=N             ; export TC_POM_JOURNALLING
# TC_JOURNAL_ROUTINE_SUMMARY_ALL=1 ; export TC_JOURNAL_ROUTINE_SUMMARY_ALL
# TC_JOURNAL_LINE_LIMIT=0          ; export TC_JOURNAL_LINE_LIMIT
# TC_Journalling=ON                ; export TC_Journalling
# TC_Journaling=ON                 ; export TC_Journaling
# TC_COMMUNICATION_LOGGING=ON      ; export TC_COMMUNICATION_LOGGING
# TC_HANDLERS_DEBUG=ALL              ; export TC_HANDLERS_DEBUG
# AM_PERFORMANCE_STATISTICS=true   ; export AM_PERFORMANCE_STATISTICS
# TC_COMMUNICATION_PROFILER=TRUE     ; export TC_COMMUNICATION_PROFILER
# TC_PERFORMANCE_MONITOR=true        ; export TC_PERFORMANCE_MONITOR
# UGII_CHECKING_LEVEL=1            ; export UGII_CHECKING_LEVEL
# API_JOURNAL=FULL                 ; export API_JOURNAL
# AM_DEBUG=ON                      ; export AM_DEBUG
# TIE_DEBUG=2                        ; export TIE_DEBUG


cd  $LOGDIR
#****************************************************
echo "Trying to start Poolmanager..."

chk_proc=`ps -ef | grep qqtegi | grep serverPool.properties | grep -v grep | wc -l`

if [ $chk_proc -ne 0 ]
then
	{
		echo "Poolmanager is already running."
	}
else
	{
		echo "Starting Poolmanager."
		nohup ${INSTDIR}/${START_EXEC} start > ${LOGDIR}/${LOGFILE} 2>&1 &
		echo "Logfile is ${LOGDIR}/${LOGFILE}"
	}
fi


echo "Done."

#****************************************************
