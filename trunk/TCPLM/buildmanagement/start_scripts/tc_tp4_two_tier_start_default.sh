#!/bin/ksh

INSTDIR=$TC_ROOT/iiopservers
START_EXEC=start_imr

#****************************************************

{
echo "Trying to start TAO..."

chk_proc=`ps -ef|grep TAO|wc -l`
echo $chk_proc
	{
	echo "Starting TAO."
	sh ${INSTDIR}/${START_EXEC}  &
	}
}

echo "Done."

