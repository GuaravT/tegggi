#!/bin/bash
######################################################################################################################
# This script is used to export the organization to xml in separated files for:
# Groups
# Roles
# This script will source the needed parameters from
# ../configurations/environment.properties
# Author: Menk Slot
# date: 2015-08-12
######################################################################################################################
# Change Nb     Who             Date            Comments
#
#
######################################################################################################################
# setting the source environment.properties file
source ../configurations/environment.properties
TC_KEEP_SYSTEM_LOG=true
plmxml_export -u=$TC_IMPORT_USER -pf=$TC_USR_PASSWORD_FILE_LOC -g=$TC_IMPORT_USER_GROUP -xml_file=T4B_TeGGGI_Groups.xml -class=Group
plmxml_export -u=$TC_IMPORT_USER -pf=$TC_USR_PASSWORD_FILE_LOC -g=$TC_IMPORT_USER_GROUP -xml_file=T4B_TeGGGI_Role.xml -class=Role
